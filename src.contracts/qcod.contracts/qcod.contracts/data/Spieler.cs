﻿using System.Collections.Generic;

using qcod.contracts.dialoge.runde;

namespace qcod.contracts.data
{
    public class Spieler
    {
        public string Id;
        public string Name;
        public List<Antworttyp> Antworten;
    }
}