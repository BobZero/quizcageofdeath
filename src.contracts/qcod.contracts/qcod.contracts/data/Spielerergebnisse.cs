﻿namespace qcod.contracts.data
{
    public class Spielerergebnisse
    {
        public string SpielerId;
        public int[] Rundenergebnisse;
        public int Gesamtergebnis;
    }
}
