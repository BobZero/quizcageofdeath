﻿using System;

namespace qcod.contracts.data
{
    public interface ITimer
    {
        void Start(TimeSpan totalTime, int updateIntervalMs, Action<TimeSpan, TimeSpan> onTimeUpdatedAction, Action onTimeElapsedAction);

        void Stop();
    }
}
