﻿namespace qcod.contracts.data
{
    public class SessionData
    {
        public string SpielerId;
        public int AktuellerRundenindex;
        public BattleData Battle;
    }
}