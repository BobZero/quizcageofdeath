﻿using qcod.contracts.dialoge.runde;

namespace qcod.contracts.data
{
    public class Frage
    {
        public string Text;
        public string[] Antwortoptionen;

        public Antworttyp Antwort { get { return Antworttyp.Antwort1; } }
    }
}