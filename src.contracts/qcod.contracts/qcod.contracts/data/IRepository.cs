﻿namespace qcod.contracts.data
{
    public interface IRepository
    {
        BattleData LoadBattle();

        void StoreBattle(BattleData battleData);
    }
}
