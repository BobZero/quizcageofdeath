﻿using System;
using System.Dynamic;
using System.Linq;

namespace qcod.contracts.data
{
    public class BattleData
    {
        public Spieler[] Spieler;
        public Frage[] Fragen;
        public int AnzahlRunden;
        public Spielzustände Status;
        public int AnzahlFragenProRunde { get { return Fragen.Length / AnzahlRunden; } }

        public dynamic Spielerfortschritt(string spielerId)
        {
            var rundenindex = -1;
            var rundenfragenindex = -1;

            var antworten = Spieler.First(s => s.Id == spielerId).Antworten;
            if (antworten.Count > 0)
            {
                rundenindex = (antworten.Count - 1) / AnzahlFragenProRunde;
                rundenfragenindex = (antworten.Count - 1) % AnzahlFragenProRunde;
            }

            dynamic fortschritt = new ExpandoObject();
            fortschritt.Rundenindex = rundenindex;
            fortschritt.Rundenfragenindex = rundenfragenindex;
            return fortschritt;
        }

        public void Fragen_eintragen(Frage[] fragen, int anzahlRunden)
        {
            if (fragen.Length % anzahlRunden > 0)
            {
                throw new ArgumentException("Anzahl der Fragen passt nicht zur Rundenzahl! "
                                            + "Gleiche Zahl von Fragen in allen Runden erforderlich.");
            }

            Fragen = fragen;
            AnzahlRunden = anzahlRunden;
        }
    }
}
