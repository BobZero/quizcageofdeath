﻿using qcod.contracts.data;

namespace qcod.contracts.dialoge.battle
{
    public class BattleInfo
    {
        public string MeinName;
        public string Gegnername;
        public Ergebnis Spielergebnis;
        public Ergebnis[] Rundenergebnisse;
        public Spielzustände Spielzustand;
    }

    public struct Ergebnis
    {
        public int MeinPunktestand;
        public int Gegnerpunktestand;
    }
}