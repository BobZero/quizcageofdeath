﻿using System;

namespace qcod.contracts.dialoge.battle
{
    public interface IBattleDialog
    {
        void Show(BattleInfo battleinfo);

        event Action RefreshRequested;
        event Action FightRequested;
        event Action SurrenderRequested;
        event Action ExitRequested;
    }
}
