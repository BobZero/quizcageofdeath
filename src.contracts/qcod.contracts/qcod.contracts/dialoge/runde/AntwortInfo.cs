﻿namespace qcod.contracts.dialoge.runde
{
    public class AntwortInfo
    {
        public Antworttyp EigeneAntwort;
        public Antworttyp RichtigeAntwort;
    }
}