﻿using System;

namespace qcod.contracts.dialoge.runde
{
    public interface IRundenDialog
    {
        void ShowFrage(FragenInfo frageninfo);
        void ShowAntworten(AntwortInfo antwortinfo);
        void ShowVerbleibendeZeit(TimeSpan verbleibendeZeit, TimeSpan gesamtZeit);

        event Action<Antworttyp> AntwortauswertungRequested;
        event Action NächsteFrageRequested;
        event Action RundenabbruchRequested;
    }
}
