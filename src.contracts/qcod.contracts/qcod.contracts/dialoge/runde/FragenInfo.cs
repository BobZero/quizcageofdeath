﻿namespace qcod.contracts.dialoge.runde
{
    public class FragenInfo
    {
        public int Fragennummer;
        public int AnzahlFragenInRunde;
        public string Fragentext;
        public string[] Antwortoptionentexte;
    }
}