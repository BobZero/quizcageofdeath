﻿namespace qcod.contracts.dialoge.runde
{
    public enum Antworttyp
    {
        KeineAntwort = -1,
        Antwort1 = 0,
        Antwort2 = 1,
        Antwort3 = 2,
        Antwort4 = 3
    }
}
