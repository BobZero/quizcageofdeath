﻿using System;
using System.Collections.Generic;

using NUnit.Framework;
using qcod.contracts.data;
using qcod.contracts.dialoge.runde;

namespace qcod.contracts.tests
{
    [TestFixture]
    public class test_BattleData
    {
        [Test]
        public void Konsistenzprüfung_Anzahl_Fragen_vs_Rundenzahl()
        {
            var sut = new BattleData();

            var fragen = new[] {new Frage(), new Frage(), new Frage(), new Frage()};
            sut.Fragen_eintragen(fragen, 2);

            Assert.Throws<ArgumentException>(() => sut.Fragen_eintragen(fragen, 3));
        }

        [Test]
        public void Fragen_pro_Runde_berechnen()
        {
            var sut = new BattleData();

            var fragen = new[] { new Frage(), new Frage(), new Frage(), new Frage() };
            sut.Fragen_eintragen(fragen, 2);

            Assert.AreEqual(2, sut.AnzahlFragenProRunde);
        }

        [Test]
        public void Spielfortschritt_ermitteln()
        {
            var sut = new BattleData();

            var fragen = new[] { new Frage(), new Frage(), new Frage(), new Frage() };
            sut.Fragen_eintragen(fragen, 2);

            var antworten = new List<Antworttyp>();
            sut.Spieler = new[] {new Spieler {Id = "1", Antworten = antworten}};

            // keine Antworten
            var fortschritt = sut.Spielerfortschritt("1");
            Assert.AreEqual(-1, fortschritt.Rundenindex);
            Assert.AreEqual(-1, fortschritt.Rundenfragenindex);

            // 1. Runde, 1. Antwort
            antworten.Add(Antworttyp.Antwort2);
            fortschritt = sut.Spielerfortschritt("1");
            Assert.AreEqual(0, fortschritt.Rundenindex);
            Assert.AreEqual(0, fortschritt.Rundenfragenindex);
        
            // 1. Runde, 2. Antwort
            antworten.Add(Antworttyp.Antwort1);
            fortschritt = sut.Spielerfortschritt("1");
            Assert.AreEqual(0, fortschritt.Rundenindex);
            Assert.AreEqual(1, fortschritt.Rundenfragenindex);
        
            // 2. Runde, 1. Antwort
            antworten.Add(Antworttyp.Antwort4);
            fortschritt = sut.Spielerfortschritt("1");
            Assert.AreEqual(1, fortschritt.Rundenindex);
            Assert.AreEqual(0, fortschritt.Rundenfragenindex);
        }
    }
}
