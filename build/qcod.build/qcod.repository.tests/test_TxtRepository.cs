﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using qcod.contracts.data;
using qcod.contracts.dialoge.runde;

using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace qcod.repository.tests
{
    [TestClass]
    public class test_TxtRepository
    {
        [TestMethod]
        public void Load()
        {
            var sut = new TxtRepository();

            var b = sut.LoadBattle();
        }

        /// <summary>
        /// Prüft ob die Antworten gemerged und nicht
        /// nur überschrieben werden
        /// </summary>
        [TestMethod]
        public void MergeStore()
        {
            var sourceData = new BattleData
            {
                Spieler = new Spieler[2],
            };
            sourceData.Spieler[0] = new Spieler { Antworten = new List<Antworttyp>() };
            sourceData.Spieler[1] = new Spieler { Antworten = new List<Antworttyp>() };
            sourceData.Spieler[0].Antworten.Add(Antworttyp.Antwort2);
            sourceData.Spieler[0].Antworten.Add(Antworttyp.Antwort3);
            sourceData.Spieler[0].Antworten.Add(Antworttyp.Antwort4);


            var targetData = new BattleData
            {
                Spieler = new Spieler[2],
            };
            targetData.Spieler[0] = new Spieler { Antworten = new List<Antworttyp>() };
            targetData.Spieler[1] = new Spieler { Antworten = new List<Antworttyp>() };
            //targetData.Spieler[1].Antworten.Add(4);
            //targetData.Spieler[1].Antworten.Add(5);
            //targetData.Spieler[1].Antworten.Add(6);

            Assert.Fail();


            var repro = new TxtRepository();
            var mergedData = repro.MergeBattleData(sourceData, targetData);

            //Assert.AreEqual(3, mergedData.Spieler[0].Antwortindizes.Count);
            //Assert.AreEqual(3, mergedData.Spieler[1].Antwortindizes.Count);

            //for (int i = 0; i < sourceData.Spieler[0].Antwortindizes.Count; i++)
            //{
            //    Assert.AreEqual(sourceData.Spieler[0].Antwortindizes[i], mergedData.Spieler[0].Antwortindizes[i]);
            //    Assert.AreEqual(targetData.Spieler[0].Antwortindizes[i], mergedData.Spieler[0].Antwortindizes[i]);
            //}

            //for (int i = 0; i < sourceData.Spieler[1].Antwortindizes.Count; i++)
            //{
            //    Assert.AreEqual(sourceData.Spieler[1].Antwortindizes[i], mergedData.Spieler[1].Antwortindizes[i]);
            //    Assert.AreEqual(targetData.Spieler[1].Antwortindizes[i], mergedData.Spieler[1].Antwortindizes[i]);
            //}
        }
    }
}
