﻿using System;

using FirstMongoSteps;

using Model;

using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace SpielDemo
{
    internal class TestDurchlauf
    {
        private static Random _rand = new Random();

        public static void Start()
        {
            //Ablauf eines Spiels mit der Kommunikation über die Db
            //=====================================================

            //Fighter A betritt den Cage
            var fighterA = Guid.NewGuid();
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("CageStore"); // "CageStore" heißt die DB
            var collection = database.GetCollection<Cage>("Cages");
            var cage1 = new Cage();
            cage1.FighterInTheLeftCornerId = fighterA;
            cage1.Status = CageStatus.Angenommen;//Offen

            collection.Insert(cage1);//Object in die DB
            
            //Fighter B betritt die Arena und sucht nach einem freien Cage
            var fighterB = Guid.NewGuid();
            var sucheLeerenCageQuery = Query<Cage>.EQ(c => c.Status, CageStatus.Offen);//Offener Cage
            var leererCage = collection.FindOne(sucheLeerenCageQuery);//Da wir hier wissen das es einen Offenen Cage gibt brauchen wir diesen eigentlich nicht explizit
            var fighterBinDenCageSteckenUpdate = Update<Cage>.Set(c => c.FighterInTheRightCornerId, fighterB);
            var cageSchließenUpdate = Update<Cage>.Set(c => c.Status, CageStatus.Angenommen);
            collection.Update(sucheLeerenCageQuery, fighterBinDenCageSteckenUpdate);
            collection.Update(sucheLeerenCageQuery, cageSchließenUpdate);


            //Der kampf kann beginnnen...
            var queryFürDenCage = Query<Cage>.EQ(c => c.Id, leererCage.Id);
            for (int i = 0; i < leererCage.Answers.Length; i++)
            {
                var answerFromTheLeftCorner = GetRandowmAnswer();
                var updateAnswerFromLeftCorner = Update<Cage>.Set(c => c.Answers[i].AnswerFromTheLeftCorner, answerFromTheLeftCorner);
                collection.Update(queryFürDenCage, updateAnswerFromLeftCorner);

                var answerFromTheRightCorner = GetRandowmAnswer();
                var updateAnswerFromRightCorner = Update<Cage>.Set(c => c.Answers[i].AnswerFromTheRightCorner, answerFromTheRightCorner);
                collection.Update(queryFürDenCage, updateAnswerFromRightCorner);
            }

            //Die Kämpfer sind müde und die Show ist vorbei.
            var cageFightBeendetUpdate = Update<Cage>.Set(c => c.Status, CageStatus.Fertig);
            collection.Update(queryFürDenCage, cageFightBeendetUpdate);

        }

        private static AnswerOption GetRandowmAnswer()
        {
            return (AnswerOption)_rand.Next(1, 4);
        }
    }
}