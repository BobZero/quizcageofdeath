﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;


namespace FirstMongoSteps
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("CageStore"); // "CageStore" heißt die DB
            var collection = database.GetCollection<Cage>("Cages");
            var cage1 = new Cage();
            cage1.FighterInTheLeftCornerId = Guid.NewGuid();
            cage1.Status = CageStatus.Angenommen;//Offen

            collection.Insert(cage1);//Object in die DB
            var cage1Id = cage1.Id;

            var query = Query<Cage>.EQ(e => e.Id, cage1Id);//Query erzeugen != DbZugriff
            var cage1FromDb = collection.FindOne(query);//Erst hier werf ich die Query auf die Db

            cage1FromDb.FighterInTheRightCornerId = Guid.NewGuid();
            cage1FromDb.Status = CageStatus.Angenommen;
            collection.Save(cage1); //save=> schiebt das komplette Object in die Db

            var update = Update<Cage>.Set(e => e.Status, CageStatus.Fertig); //erzeugt eine UpdateQuery => noch kein Db-Zugriff
            collection.Update(query, update);//update=> schiebt nur die Änderung in die Db

            collection.Remove(query); //und wieder raus.
        }
    }
}
