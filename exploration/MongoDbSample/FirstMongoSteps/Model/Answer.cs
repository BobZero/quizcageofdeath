﻿namespace Model
{
    public class Answer
    {
        public AnswerOption AnswerFromTheRightCorner { get; set; }
        public AnswerOption AnswerFromTheLeftCorner { get; set; }
    }
}