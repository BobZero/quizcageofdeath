﻿using System;

using MongoDB.Bson;

namespace Model
{
    public class Cage
    {
        public Cage()
        {
            Questions = new Question[18];
            Answers = new Answer[18];
            for (int index = 0; index < Questions.Length; index++)
            {
                Questions[index] = new Question();
                Answers[index] = new Answer();
            }
        }

        public ObjectId Id { get; set; }//Muss Id heißen sonst vergibt die DbEngine irgendwie keine Ids

        public Guid FighterInTheLeftCornerId { get; set; }

        public Guid FighterInTheRightCornerId { get; set; }

        public Question[] Questions { get; set; }

        public Answer[] Answers { get; set; }

        public CageStatus Status { get; set; }
    }

    public enum CageStatus
    {
        Offen,
        Angenommen,
        Fertig
    }
}