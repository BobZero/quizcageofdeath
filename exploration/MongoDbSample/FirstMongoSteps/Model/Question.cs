﻿using MongoDB.Bson;

namespace Model
{
    public class Question
    {
        public ObjectId Id { get; set; }
        public string Frage { get; set; }
        public string Antwort { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option4 { get; set; }
    }
}
