﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Model
{
    public class FighterBot
    {
        private static Random _rand = new Random();
        private Guid _figtherId;

        public FighterBot()
        {
            _figtherId = Guid.NewGuid();
        }
        public void Start()
        {
            var cage = new CageModel();
            cage.JoinCage(_figtherId);
            var answersGiven = 0;
            while (answersGiven<18)
            {
                if (cage.CanAnswer(_figtherId))
                {
                    cage.AnswerQuestion(GetRandowmAnswer());
                    answersGiven++;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        private AnswerOption GetRandowmAnswer()
        {
            return (AnswerOption)_rand.Next(1, 4);
        }
    }
}
