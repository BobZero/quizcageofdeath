﻿using System;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Model
{
    public class CageModel
    {
        private MongoCollection<Cage> _collection = null;
        private ObjectId _cageId = new ObjectId();
        private Corner _myCorner;
        
        public CageModel()
        {
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("CageStore");
            _collection = database.GetCollection<Cage>("Cages");
        }

        public void JoinCage(Guid fighterId)
        {
            var sucheLeerenCageQuery = Query<Cage>.EQ(c => c.Status, CageStatus.Offen);
            var leererCage = _collection.FindOne(sucheLeerenCageQuery);
            if (leererCage != null)
            {
                var addUserUpdate = Update<Cage>.Set(c => c.FighterInTheRightCornerId, fighterId);
                var closeCageUpdate = Update<Cage>.Set(c => c.Status, CageStatus.Angenommen);
                _collection.Update(sucheLeerenCageQuery, addUserUpdate);
                _collection.Update(sucheLeerenCageQuery, closeCageUpdate);
                _myCorner = Corner.Right;
                _cageId = leererCage.Id;
            }
            else
            {
                var newCage = new Cage();
                newCage.Status = CageStatus.Offen;
                newCage.FighterInTheLeftCornerId = fighterId;
                _collection.Insert(newCage);
                _myCorner = Corner.Left;
                _cageId = newCage.Id;
            }
        }

        public void AnswerQuestion(AnswerOption answerOption)
        {
            var currentCage = GetCurrentCage();
            foreach (var answer in currentCage.Answers)
            {
                if (_myCorner == Corner.Left)
                {
                    if (answer.AnswerFromTheLeftCorner == AnswerOption.Offen)
                    {
                        answer.AnswerFromTheLeftCorner = answerOption;
                        _collection.Save(currentCage);
                        break;
                    }
                }
                else
                {
                    if (answer.AnswerFromTheRightCorner == AnswerOption.Offen)
                    {
                        answer.AnswerFromTheRightCorner = answerOption;
                        _collection.Save(currentCage);
                        break;
                    }

                }
            }
            EndFight();
        }

        private void EndFight()
        {
            var currentCage = GetCurrentCage();
            var fightFinished = true;
            foreach (var answer in currentCage.Answers)
            {
                if (answer.AnswerFromTheLeftCorner == AnswerOption.Offen || answer.AnswerFromTheRightCorner == AnswerOption.Offen)
                {
                    fightFinished = false;
                    break;
                }
            }
            if (fightFinished)
            {
                currentCage.Status = CageStatus.Fertig;
                _collection.Save(currentCage);
            }
        }

        public bool CanAnswer(Guid fighterId)
        {
            var retVal = false;
            var answers = GetCurrentCage().Answers;
            foreach (var answer in answers)
            {
                if (_myCorner == Corner.Left)
                {
                    if (answer.AnswerFromTheLeftCorner == AnswerOption.Offen)
                    {
                        retVal = true;
                        break;
                    }
                    else
                    {
                        if (answer.AnswerFromTheRightCorner == AnswerOption.Offen)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (answer.AnswerFromTheRightCorner == AnswerOption.Offen)
                    {
                        retVal = true;
                        break;
                    }
                    else
                    {
                        if (answer.AnswerFromTheLeftCorner == AnswerOption.Offen)
                        {
                            break;
                        }
                    }
                    
                }
            }
            return retVal;
        }

        private Cage GetCurrentCage()
        {
            var getCurrentCageQuery = Query<Cage>.EQ(c => c.Id, _cageId);
            return _collection.FindOne(getCurrentCageQuery);
        }
    }
}
