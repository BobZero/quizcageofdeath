﻿using System.Threading;
using System.Threading.Tasks;

using Model;

namespace BotParty
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10000; i++)
            {
                Task.Factory.StartNew(() => new FighterBot().Start());
            }


            Task.Factory.StartNew(() => new FighterBot().Start()).Wait();


            Thread.Sleep(10000);
        }
    }
}
