﻿using System;
using qcod.contracts.data;
using qcod.contracts.dialoge.battle;
using qcod.contracts.dialoge.runde;
using qcod.mapping;
using qcod.model;

namespace qcod.interaktionen
{
    public class Interaktionen
    {
        private static readonly TimeSpan Beantwortungszeit = new TimeSpan(0, 0, 0, 20);

        private const int AktualisierungsRateInMillisekunden = 500;

        private readonly SessionData _session;
        private readonly Battle _battle;
        private readonly BattleMapper _map;
        private readonly IRepository _repo;
        private readonly ITimer _timer;

        public event Action<BattleInfo> OnBattleInfo;
        public event Action<FragenInfo> OnFragenInfo;
        public event Action<AntwortInfo> OnAntwortInfo;
        public event Action<TimeSpan, TimeSpan> OnVerbleibendeZeit;

        public Interaktionen(SessionData session, Battle battle, BattleMapper map, IRepository repo, ITimer timer)
        {
            _session = session;
            _battle = battle;
            _map = map;
            _repo = repo;
            _timer = timer;
        }

        #region dlg battle

        public void Starten(string spielerId)
        {
            // Attrappenimpl
            _session.SpielerId = spielerId;
            _session.Battle = _repo.LoadBattle();
            _battle.Binden(_session.Battle);
            _map.Binden(_session.Battle);
            BattleInfo_generieren();
        }

        public void HandleRefreshRequest()
        {
            _session.Battle = _repo.LoadBattle();
            _battle.Binden(_session.Battle);
            _map.Binden(_session.Battle);
            BattleInfo_generieren();
        }

        public void HandleSurrenderRequest()
        {
            // Attrappenimplementation
            _session.Battle.Status = Spielzustände.Beendet;
            HandleRefreshRequest();
        }

        public void HandleExitRequest()
        {
            // Attrappenimplementation
            Environment.Exit(0);
        }

        public void HandleFightRequest()
        {
            _battle.Nächste_Rundenindex_berechnen(_session.SpielerId,
                i =>
                {
                    _session.AktuellerRundenindex = i;
                    HandleNächsteFrageRequest();
                },
                HandleRefreshRequest);
        }

        #endregion

        #region dlg runden

        public void HandleAntwortauswertungRequest(Antworttyp antwort)
        {
            _timer.Stop();
            OnVerbleibendeZeit(TimeSpan.Zero, TimeSpan.Zero);

            if (antwort != Antworttyp.KeineAntwort)
            {
                antwort = (Antworttyp)_map.Antwortindex_entmischen((int)antwort);
            }

            _battle.Antwort_eintragen(antwort, _session.SpielerId);
            _repo.StoreBattle(_session.Battle);

            var lastAnsweredQuestionIndex = _battle.Index_der_zuletzt_beantworteten_Frage(_session.SpielerId);

            var antwortInfo = _map.AntwortInfo_generieren(lastAnsweredQuestionIndex, _session.SpielerId);
            antwortInfo = _map.Antwortinfo_mischen(antwortInfo);
            OnAntwortInfo(antwortInfo);
        }

        public void HandleNächsteFrageRequest()
        {
            _battle.Index_der_nächsten_Frage(_session.AktuellerRundenindex, _session.SpielerId,
                i =>
                {
                    var fi = _map.FragenInfo_generieren(i);
                    fi.Antwortoptionentexte = _map.Antwortoptionen_mischen(fi.Antwortoptionentexte);
                    OnFragenInfo(fi);
                    _timer.Start(
                        Beantwortungszeit,
                        AktualisierungsRateInMillisekunden,
                        OnVerbleibendeZeit,
                        () => HandleAntwortauswertungRequest(Antworttyp.KeineAntwort));
                },
                BattleInfo_generieren);
        }

        public void HandleRundenabbruchRequest()
        {
            _timer.Stop();
            HandleRefreshRequest();
        }

        #endregion

        private void BattleInfo_generieren()
        {
            var ergebnisse = _battle.Ergebnisstand_berechnen();
            var bi = _map.BattleInfo_generieren(_session.SpielerId, ergebnisse);
            OnBattleInfo(bi);
        }
    }
}