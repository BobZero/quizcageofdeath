﻿using System;
using System.Diagnostics;
using System.Timers;

using qcod.contracts.data;

namespace qcod.TimeProvider
{
    public class Timer : ITimer
    {
        private System.Timers.Timer _timer;

        private Stopwatch _stopwatch;

        private TimeSpan _totalTime;

        private Action<TimeSpan, TimeSpan> _onTimeUpdatedAction;
        private Action _onTimeElapsedAction;

        private bool _started;

        public void Start(TimeSpan totalTime, int updateIntervalMs, Action<TimeSpan, TimeSpan> onTimeUpdatedAction, Action onTimeElapsedAction)
        {
            if (_started)
            {
                Stop();
            }

            _totalTime = totalTime;
            _onTimeUpdatedAction = onTimeUpdatedAction;
            _onTimeElapsedAction = onTimeElapsedAction;

            _stopwatch = new Stopwatch();
            _timer = new System.Timers.Timer(updateIntervalMs);
            _timer.Elapsed += OnTimerElapsed;

            _timer.Start();
            _stopwatch.Start();

            _started = true;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _stopwatch.Stop();
            _timer.Stop();

            if (_stopwatch.Elapsed >= _totalTime)
            {
                _onTimeElapsedAction();
            }
            else
            {
                var remainingTime = _totalTime - _stopwatch.Elapsed;
                _onTimeUpdatedAction(remainingTime, _totalTime);

                _timer.Start();
                _stopwatch.Start();
            }
        }

        public void Stop()
        {
            if (!_started)
            {
                return;
            }

            _stopwatch.Stop();
            _stopwatch = null;

            _timer.Stop();
            _timer.Elapsed -= OnTimerElapsed;
            _timer = null;

            _started = false;
        }
    }
}
