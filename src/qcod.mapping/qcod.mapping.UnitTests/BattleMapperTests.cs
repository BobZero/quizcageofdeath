﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using equalidator;
using qcod.contracts.data;
using qcod.contracts.dialoge.runde;

namespace qcod.mapping.UnitTests
{
    [TestClass]
    public class BattleMapperTests
    {
        [TestMethod]
        public void AntwortInfo_generieren_Test()
        {
            var fragenindex = 3;
            var spielerId = "123";
            var erwarteterIndexDerEigenenAntwort = Antworttyp.Antwort4;

            var data = new BattleData
                {
                    Spieler = new[]
                        {
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp>
                                                    {
                                                        Antworttyp.Antwort2, 
                                                        Antworttyp.Antwort4, 
                                                        Antworttyp.Antwort3, 
                                                        erwarteterIndexDerEigenenAntwort
                                                    },
                                    Id = spielerId,
                                },
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp>
                                                    {
                                                        Antworttyp.Antwort2, 
                                                        Antworttyp.Antwort2, 
                                                        Antworttyp.Antwort2, 
                                                        Antworttyp.Antwort2
                                                    },
                                    Id = "124",
                                }
                        },
                };
            data.Fragen_eintragen(new[] { new Frage(), new Frage(), new Frage(), new Frage() }, 1);

            var battleMapper = new BattleMapper();
            battleMapper.Binden(data);
            var antwortInfo = battleMapper.AntwortInfo_generieren(fragenindex, spielerId);

            Assert.AreEqual(0, antwortInfo.RichtigeAntwort);
            Assert.AreEqual(erwarteterIndexDerEigenenAntwort, antwortInfo.EigeneAntwort);
        }

        [TestMethod]
        public void FragenInfo_generieren()
        {
            var data = new BattleData();
            data.Fragen_eintragen(new[] { new Frage { Text = "t", Antwortoptionen = new[] { "x", "y" } } }, 1);

            var battleMapper = new BattleMapper();
            battleMapper.Binden(data);
            var fragenInfo = battleMapper.FragenInfo_generieren(0);

            Assert.AreEqual(1, fragenInfo.Fragennummer);
            Assert.AreEqual("t", fragenInfo.Fragentext);
            Assert.AreEqual(1, fragenInfo.AnzahlFragenInRunde);
            Assert.AreEqual("x", fragenInfo.Antwortoptionentexte[0]);
            Assert.AreEqual("y", fragenInfo.Antwortoptionentexte[1]);
        }

        [TestMethod]
        public void Fragennummer_in_Runde_berechnen()
        {
            Assert.AreEqual(1, BattleMapper.Fragennummer_in_Runde_berechnen_1based(0, 3));
            Assert.AreEqual(3, BattleMapper.Fragennummer_in_Runde_berechnen_1based(2, 3));
            Assert.AreEqual(1, BattleMapper.Fragennummer_in_Runde_berechnen_1based(3, 3));
        }

        [TestMethod]
        public void BattleInfo_generieren_Test()
        {
            var data = new BattleData
                {
                    Spieler = new[]
                        {
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp> {Antworttyp.Antwort2},
                                    Id = "123",
                                    Name = "Spieler1"
                                },
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp> {Antworttyp.Antwort1},
                                    Id = "124",
                                    Name = "Spieler2"
                                }
                        },
                    Status = Spielzustände.Angenommen
                };
            data.Fragen_eintragen(new[] { new Frage() }, 1);

            var ergebnisse = new[] {
                new Spielerergebnisse{SpielerId = "123",Rundenergebnisse = new[]{0},Gesamtergebnis = 0},
                new Spielerergebnisse{SpielerId = "124",Rundenergebnisse = new[]{1},Gesamtergebnis = 1},
            };

            var battleMapper = new BattleMapper();
            battleMapper.Binden(data);
            var battleInfo = battleMapper.BattleInfo_generieren("123", ergebnisse);

            Assert.AreEqual("Spieler2", battleInfo.Gegnername);
            Assert.AreEqual("Spieler1", battleInfo.MeinName);
            Assert.AreEqual(0, battleInfo.Rundenergebnisse[0].MeinPunktestand);
            Assert.AreEqual(1, battleInfo.Rundenergebnisse[0].Gegnerpunktestand);
            Assert.AreEqual(0, battleInfo.Spielergebnis.MeinPunktestand);
            Assert.AreEqual(1, battleInfo.Spielergebnis.Gegnerpunktestand);
            Assert.AreEqual(Spielzustände.Angenommen, battleInfo.Spielzustand);
        }

        [TestMethod]
        public void Aktuellen_Spieler_links_anzeigen()
        {
            var data = new BattleData
                {
                    Spieler = new[]
                        {
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp> {Antworttyp.Antwort2},
                                    Id = "123",
                                    Name = "Spieler1"
                                },
                            new Spieler
                                {
                                    Antworten = new List<Antworttyp> {Antworttyp.Antwort1},
                                    Id = "124",
                                    Name = "Spieler2"
                                }
                        },
                    Status = Spielzustände.Angenommen
                };
            data.Fragen_eintragen(new[] { new Frage() }, 1);

            var ergebnisse = new[] {
                new Spielerergebnisse{SpielerId = "124",Rundenergebnisse = new[]{0},Gesamtergebnis = 0},
                new Spielerergebnisse{SpielerId = "123",Rundenergebnisse = new[]{1},Gesamtergebnis = 1},
            };

            var battleMapper = new BattleMapper();
            battleMapper.Binden(data);
            var battleInfo = battleMapper.BattleInfo_generieren("124", ergebnisse);

            Assert.AreEqual("Spieler1", battleInfo.Gegnername);
            Assert.AreEqual("Spieler2", battleInfo.MeinName);
            Assert.AreEqual(1, battleInfo.Rundenergebnisse[0].MeinPunktestand);
            Assert.AreEqual(0, battleInfo.Rundenergebnisse[0].Gegnerpunktestand);
            Assert.AreEqual(1, battleInfo.Spielergebnis.MeinPunktestand);
            Assert.AreEqual(0, battleInfo.Spielergebnis.Gegnerpunktestand);
        }

        [TestMethod]
        public void Rundenergebnisse_zusammenmischen()
        {
            var ergebnis = BattleMapper.Rundenergebnisse_der_Spieler_zusammenmischen(new[] { 1 }, new[] { 0, 2 });

            Assert.AreEqual(0, ergebnis[0].MeinPunktestand);
            Assert.AreEqual(1, ergebnis[0].Gegnerpunktestand);
            Assert.AreEqual(2, ergebnis[1].MeinPunktestand);
            Assert.AreEqual(0, ergebnis[1].Gegnerpunktestand);
        }

        [TestMethod]
        public void Antwortoptionen_mischen()
        {
            var sut = new BattleMapper();

            var original = new[] { "a", "b", "c", "d" };
            try
            {
                for (var i = 0; i < 10; i++)
                {
                    var gemischt = sut.Antwortoptionen_mischen(original);
                    Console.WriteLine("gemischte Antworten: {0}", string.Join(",", gemischt));
                    Equalidator.AreEqual(original, gemischt);
                }
                Assert.Fail(); // alle 10 Mischungen waren wie das Original
            }
            catch (equalidator.exceptions.NotEqualDueToDifferentValues)
            {
                // zumindest eines Mischung unterschied sich vom Original   
            }
        }

        [TestMethod]
        public void Antwortoptionen_umstellen()
        {
            var sut = new BattleMapper();
            var mi = typeof(BattleMapper).GetMethod("Antwortoptionen_umstellen", BindingFlags.Instance | BindingFlags.NonPublic);

            var gemischt = (string[])mi.Invoke(sut, new object[] { new[] { "2", "11", "10", "3" }, new[] { 3, 1, 0, 2 } });

            Equalidator.AreEqual(new[] { "3", "11", "2", "10" }, gemischt);
        }

        [TestMethod]
        public void Antwortindex_entmischen()
        {
            var sut = new BattleMapper();
            var mi = typeof(BattleMapper).GetMethod("Antwortoptionen_umstellen", BindingFlags.Instance | BindingFlags.NonPublic);

            mi.Invoke(sut, new object[] { new[] { "a", "b", "c", "d" }, new[] { 3, 2, 1, 0 } });
            Assert.AreEqual(1, sut.Antwortindex_entmischen(2));
        }
    }
}