﻿using System;
using System.Collections.Generic;
using System.Linq;
using qcod.contracts.data;
using qcod.contracts.dialoge.battle;
using qcod.contracts.dialoge.runde;

namespace qcod.mapping
{
    public class BattleMapper
    {
        private BattleData _data;
        public void Binden(BattleData data) { _data = data; }


        public AntwortInfo AntwortInfo_generieren(int fragenindex, string spielerId)
        {
            var spieler = _data.Spieler.First(s => s.Id == spielerId);
            var antwortInfo = new AntwortInfo
            {
                EigeneAntwort = spieler.Antworten[fragenindex],
                RichtigeAntwort = _data.Fragen[fragenindex].Antwort
            };
            return antwortInfo;
        }


        public FragenInfo FragenInfo_generieren(int fragenindex)
        {
            var frage = _data.Fragen[fragenindex];

            var fragenInfo = new FragenInfo
            {
                Fragentext = frage.Text,
                Antwortoptionentexte = frage.Antwortoptionen,
                AnzahlFragenInRunde = _data.AnzahlFragenProRunde
            };
            fragenInfo.Fragennummer = Fragennummer_in_Runde_berechnen_1based(fragenindex, fragenInfo.AnzahlFragenInRunde);

            return fragenInfo;
        }

        internal static int Fragennummer_in_Runde_berechnen_1based(int fragenindex, int anzahlFragenInRunde)
        {
            return (fragenindex % anzahlFragenInRunde) + 1;
        }


        public BattleInfo BattleInfo_generieren(string spielerId, Spielerergebnisse[] ergebnisse)
        {
            int iSpieler, iGegner;
            if (_data.Spieler[0].Id == spielerId) { iSpieler = 0; iGegner = 1; }
            else { iSpieler = 1; iGegner = 0; }

            var battleInfo = new BattleInfo
            {
                MeinName = _data.Spieler[iSpieler].Name,
                Gegnername = _data.Spieler[iGegner].Name,
                Rundenergebnisse = Rundenergebnisse_der_Spieler_zusammenmischen(ergebnisse[iGegner].Rundenergebnisse, ergebnisse[iSpieler].Rundenergebnisse),
                Spielergebnis = new Ergebnis { MeinPunktestand = ergebnisse[iSpieler].Gesamtergebnis, Gegnerpunktestand = ergebnisse[iGegner].Gesamtergebnis },
                Spielzustand = _data.Status,
            };

            return battleInfo;
        }

        internal static Ergebnis[] Rundenergebnisse_der_Spieler_zusammenmischen(int[] gegnerRundenErgebnisse, int[] spielerRundenErgebnisse)
        {
            var ergebnisse = new List<Ergebnis>();
            for (var i = 0; i < Math.Max(gegnerRundenErgebnisse.Length, spielerRundenErgebnisse.Length); i++)
            {
                ergebnisse.Add(new Ergebnis
                {
                    MeinPunktestand = i < spielerRundenErgebnisse.Length ? spielerRundenErgebnisse[i] : 0,
                    Gegnerpunktestand = i < gegnerRundenErgebnisse.Length ? gegnerRundenErgebnisse[i] : 0
                });
            }
            return ergebnisse.ToArray();
        }


        private int[] _indexMap_gemischter_Antworten;

        public string[] Antwortoptionen_mischen(string[] antwortoptionentexte)
        {
            var indexMap = Antwortindizes_mischen(antwortoptionentexte.Length);
            return Antwortoptionen_umstellen(antwortoptionentexte, indexMap);
        }

        private static int[] Antwortindizes_mischen(int nIndizes)
        {
            var indexMap = new List<int>();
            var rnd = new Random();
            while (indexMap.Count < nIndizes)
            {
                var i = rnd.Next(0, nIndizes);
                if (!indexMap.Contains(i)) indexMap.Add(i);
            }
            return indexMap.ToArray();
        }

        private string[] Antwortoptionen_umstellen(string[] antwortoptionentexte, int[] indexMap)
        {
            var gemischt = new string[antwortoptionentexte.Length];
            for (var i = 0; i < antwortoptionentexte.Length; i++)
                gemischt[i] = antwortoptionentexte[indexMap[i]];
            _indexMap_gemischter_Antworten = indexMap;
            return gemischt;
        }


        public int Antwortindex_entmischen(int antwortIndex)
        {
            return _indexMap_gemischter_Antworten[antwortIndex];
        }


        public AntwortInfo Antwortinfo_mischen(AntwortInfo antwortInfo)
        {
            return new AntwortInfo
            {
                EigeneAntwort =
                (Antworttyp)_indexMap_gemischter_Antworten
                        .Select((i, p) => new { Index = i, Position = p })
                        .Where(t => t.Index == (int)antwortInfo.EigeneAntwort)
                        .Select(t => t.Position).First(),
                RichtigeAntwort =
                    (Antworttyp)_indexMap_gemischter_Antworten
                        .Select((i, p) => new { Index = i, Position = p })
                        .Where(t => t.Index == (int)antwortInfo.RichtigeAntwort)
                        .Select(t => t.Position).First()
            };
        }
    }
}