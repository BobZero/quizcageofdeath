﻿using System;
using System.Windows;

using qcod.contracts.data;
using qcod.interaktionen;
using qcod.mapping;
using qcod.model;
using qcod.repository;
using qcod.TimeProvider;
using qcod.ui.wpf;

namespace qcod.application.wpf
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (e.Args.Length < 1)
            {
                MessageBox.Show("Parameter Spieler ID fehlt!");
                Environment.Exit(0);
            }

            var session = new SessionData();

            var timer = new Timer();
            var repo = new TxtRepository();
            var userInterface = new UserInterface();

            var battle = new Battle();
            var map = new BattleMapper();

            var interaktionen = new Interaktionen(session, battle, map, repo, timer);
            userInterface.FightRequested += interaktionen.HandleFightRequest;
            userInterface.RefreshRequested += interaktionen.HandleRefreshRequest;
            userInterface.SurrenderRequested += interaktionen.HandleSurrenderRequest;
            userInterface.ExitRequested += interaktionen.HandleExitRequest;

            interaktionen.OnBattleInfo += userInterface.Show;

            userInterface.AntwortauswertungRequested += interaktionen.HandleAntwortauswertungRequest;
            userInterface.NächsteFrageRequested += interaktionen.HandleNächsteFrageRequest;
            userInterface.RundenabbruchRequested += interaktionen.HandleRundenabbruchRequest;

            interaktionen.OnFragenInfo += userInterface.ShowFrage;
            interaktionen.OnAntwortInfo += userInterface.ShowAntworten;
            interaktionen.OnVerbleibendeZeit += userInterface.ShowVerbleibendeZeit;
            
            interaktionen.Starten(e.Args[0]);
        }
    }
}
