﻿using System.Collections.Generic;
using qcod.contracts.data;
using System;
using System.Linq;

using qcod.contracts.dialoge.runde;

namespace qcod.model
{
    public class Battle
    {
        private BattleData _data;
        public void Binden(BattleData data) { _data = data; }

        
        public Spielerergebnisse[] Ergebnisstand_berechnen()
        {
            var ergebnisse = new List<Spielerergebnisse>();

            foreach (var spieler in _data.Spieler)
            {
                var se = new Spielerergebnisse 
                {
                    SpielerId = spieler.Id,
                    Rundenergebnisse = Rundenergebnisse_aus_Antworten_berechnen(spieler.Antworten).ToArray()
                };
                se.Gesamtergebnis = se.Rundenergebnisse.Sum();
                ergebnisse.Add(se);
            }

            return ergebnisse.ToArray();
        }
        
        public void Nächste_Rundenindex_berechnen(string spielerId, Action<int> onNächsteRunde, Action onKeineNächsteRunde)
        {
            var seinFortschritt = _data.Spielerfortschritt(spielerId);
            var gegnerischeSpielerId = _data.Spieler.First(s => s.Id != spielerId).Id;
            var gegnerischerFortschritt = _data.Spielerfortschritt(gegnerischeSpielerId);

            if (seinFortschritt.Rundenindex <= gegnerischerFortschritt.Rundenindex
                && seinFortschritt.Rundenindex < _data.AnzahlRunden - 1)
            {
                onNächsteRunde(seinFortschritt.Rundenindex + 1);
            }
            else
            {
                onKeineNächsteRunde();
            }
        }
        
        public int Index_der_zuletzt_beantworteten_Frage(string spielerId)
        {
            return GetAntworten(spielerId).Count - 1;
        }
        
        public void Index_der_nächsten_Frage(int rundenindex, string spielerId, Action<int> onNächsteFrage, Action onKeineFrageMehr)
        {
            var antwortIndizes = GetAntworten(spielerId);
            var anzahl_fragen_pro_runde = _data.Fragen.Count()/_data.AnzahlRunden;
            var max_anzahl_antworten_am_ende_der_runde = (rundenindex + 1)*anzahl_fragen_pro_runde;

            if (antwortIndizes.Count < max_anzahl_antworten_am_ende_der_runde)
            {
                onNächsteFrage(antwortIndizes.Count);
            }
            else
            {
                onKeineFrageMehr();
            }
        }
        
        public void Antwort_eintragen(Antworttyp antwort, string spielerId)
        {
            var antworten = GetAntworten(spielerId);
            antworten.Add(antwort);
        }

        private List<Antworttyp> GetAntworten(string spielerId)
        {
            var spieler = _data.Spieler.First(s => s.Id == spielerId);
            return spieler.Antworten;
        }
        
        private List<int> Rundenergebnisse_aus_Antworten_berechnen(List<Antworttyp> antworten)
        {
            var rundenErgebnisse = new List<int>();
            var anzahlRichtigeInRunde = 0;
            var fragennummer = 0;

            for (var i = 0; i < antworten.Count; i++)
            {
                var antwort = antworten[i];
                var richtigeAntwort = _data.Fragen[i].Antwort;

                fragennummer++;

                if (antwort == richtigeAntwort)
                {
                    anzahlRichtigeInRunde++;
                }

                if (fragennummer != _data.AnzahlFragenProRunde)
                {
                    continue;
                }

                rundenErgebnisse.Add(anzahlRichtigeInRunde);
                anzahlRichtigeInRunde = 0;
                fragennummer = 0;
            }

            if (fragennummer > 0)
            {
                rundenErgebnisse.Add(anzahlRichtigeInRunde);
            }

            return rundenErgebnisse;
        }
    }
}
