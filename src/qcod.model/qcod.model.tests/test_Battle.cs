﻿using System;
using qcod.contracts.data;
using qcod.contracts.dialoge.battle;
using qcod.model;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace qcod.model.tests
{
    [TestClass]
    public class test_Battle
    {
        [TestMethod]
        public void Rundenergebnisse_berechnen()
        {
            var data = new BattleData();
            data.Fragen_eintragen(new[] {new Frage(), new Frage(), new Frage(), new Frage(), new Frage(), new Frage()}, 3);

            var battle = new Battle();
            battle.Binden(data);
            var rundenErgebnisseEinesSpielers = battle.Rundenergebnisse_aus_Antworten_berechnen(new List<int> {0, 0, 1, 2, 0});

            Assert.AreEqual(2, rundenErgebnisseEinesSpielers[0]);
            Assert.AreEqual(0, rundenErgebnisseEinesSpielers[1]);
            Assert.AreEqual(1, rundenErgebnisseEinesSpielers[2]);
        }
    }
}
