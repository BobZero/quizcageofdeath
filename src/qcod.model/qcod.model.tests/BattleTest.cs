﻿using System.Linq;

using qcod.contracts.data;

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using qcod.contracts.dialoge.runde;

namespace qcod.model.tests
{
    [TestClass]
    public class BattleTest
    {
        [TestMethod]
        public void Ergebnisstand_berechnenTest()
        {
            const string id1 = "1";
            const string id2 = "2";

            const int roundCount = 3;

            var antworten1 = new List<Antworttyp>
                                 {
                                     Antworttyp.Antwort1,
                                     Antworttyp.Antwort2, 
                                     Antworttyp.Antwort2, 
                                     Antworttyp.Antwort1
                                 };
            var antworten2 = new List<Antworttyp>
                                 {
                                     Antworttyp.Antwort1, 
                                     Antworttyp.Antwort1, 
                                     Antworttyp.Antwort1, 
                                     Antworttyp.Antwort1
                                 };

            var battleData = new BattleData
            {
                Spieler = new[]
                                {
                                    new Spieler
                                        {
                                            Id = id1, 
                                            Antworten = antworten1
                                        },
                                    new Spieler
                                        {
                                            Id = id2, 
                                            Antworten = antworten2
                                        }
                                    }
            };

            battleData.Fragen_eintragen(new[]
                                            {
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage()
                                            }, roundCount);

            var battle = new Battle();
            battle.Binden(battleData);

            var result = battle.Ergebnisstand_berechnen();

            Assert.AreEqual(2, result[0].Gesamtergebnis, "Gesamtergebnis falsch berechnet!");
            Assert.AreEqual(1, result[0].Rundenergebnisse[0], "Rundenergebnis falsch berechnet!");
            Assert.AreEqual(1, result[0].Rundenergebnisse[1], "Rundenergebnis falsch berechnet!");

            Assert.AreEqual(4, result[1].Gesamtergebnis, "Gesamtergebnis falsch berechnet!");
            Assert.AreEqual(2, result[1].Rundenergebnisse[0], "Rundenergebnis falsch berechnet!");
            Assert.AreEqual(2, result[1].Rundenergebnisse[1], "Rundenergebnis falsch berechnet!");
        }

        [TestMethod]
        public void Nächste_Rundenindex_berechnenTestNextRound()
        {
            const string id1 = "1";
            const string id2 = "2";

            const int roundCount = 3;

            var antworten1 = new List<Antworttyp> { Antworttyp.Antwort2, Antworttyp.Antwort3 };
            var antworten2 = new List<Antworttyp>();
            //var antworten2 = new List<Antworttyp> { 4, 5 };
            Assert.Fail("//var antworten2 = new List<Antworttyp> { 4, 5 };");

            var battleData = new BattleData
                                 {
                                     Spieler = new[]
                                                   {
                                                       new Spieler
                                                           {
                                                               Id = id1, 
                                                               Antworten = antworten1
                                                           },
                                                       new Spieler
                                                           {
                                                               Id = id2, 
                                                               Antworten = antworten2
                                                           }
                                                   }
                                 };

            battleData.Fragen_eintragen(new[]
                                            {
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage(), 
                                                new Frage()
                                            }, roundCount);

            var battle = new Battle();
            battle.Binden(battleData);
            var result = 0;

            battle.Nächste_Rundenindex_berechnen(id1, (index) => result = index, null);

            Assert.AreEqual(1, result, "Falscher Index für die nächste Runde!");
        }

        [TestMethod]
        public void Nächste_Rundenindex_berechnenTestNoNextRound()
        {
            Assert.Fail();
            const string id1 = "1";
            const string id2 = "2";

            const int roundCount = 2;

            //var indices1 = new List<int> { 1, 2, 3, 4 };
            //var indices2 = new List<int> { 5, 6, 7, 5 };

            //var battleData = new BattleData
            //{
            //    Spieler = new[]
            //                                       {
            //                                           new Spieler
            //                                               {
            //                                                   Id = id1, 
            //                                                   Antworten = indices1
            //                                               },
            //                                           new Spieler
            //                                               {
            //                                                   Id = id2, 
            //                                                   Antworten = indices2
            //                                               }
            //                                       }
            //};

            //battleData.Fragen_eintragen(new[]
            //                                {
            //                                    new Frage(), 
            //                                    new Frage(), 
            //                                    new Frage(), 
            //                                    new Frage()
            //                                }, roundCount);

            //var battle = new Battle();
            //battle.Binden(battleData);
            var nextRound = true;

            //battle.Nächste_Rundenindex_berechnen(id1, null, () => nextRound = false);

            Assert.IsFalse(nextRound, "Es wurde nicht ermittelt, dass keine Runde mehr gespielt wird!");
        }

        [TestMethod]
        public void Index_der_zuletzt_beantworteten_FrageTest()
        {
            const string id = "1";
            var answerIndices = new List<int> { 3, 4, 5 };

            Assert.Fail();

            //var battleData = new BattleData
            //                     {
            //                         Spieler = new[]
            //                                       {
            //                                           new Spieler
            //                                               {
            //                                                   Id = id,
            //                                                   Antwortindizes = answerIndices
            //                                               }
            //                                       }
            //                     };
            //var battle = new Battle();
            //battle.Binden(battleData);

            //var result = battle.Index_der_zuletzt_beantworteten_Frage(id);

            //Assert.AreEqual(answerIndices.Count - 1, result, "Index der zuletzt beantworteten Frage nicht korrekt ermittelt!");
        }

        [TestMethod]
        public void Index_der_nächsten_FrageTestNextQuestion()
        {
            const string id1 = "1";
            const string id2 = "2";

            const int roundCount = 2;
            const int roundIndex = 1;
            const int expectedQuestionIndex = 2;

            var indices1 = new List<int> { 1, 2 };
            var indices2 = new List<int> { 4, 5 };

            Assert.Fail();

            //var battleData = new BattleData
            //                     {
            //                         Spieler = new[]
            //                                      {
            //                                          new Spieler
            //                                              {
            //                                                  Id = id1, 
            //                                                  Antwortindizes = indices1
            //                                              },
            //                                          new Spieler
            //                                              {
            //                                                  Id = id2, 
            //                                                  Antwortindizes = indices2
            //                                              }
            //                                      }
            //                     };

            //battleData.Fragen_eintragen(new[] { new Frage(), new Frage(), new Frage(), new Frage() }, roundCount);

            //var battle = new Battle();
            //battle.Binden(battleData);
            //var result = 0;

            //battle.Index_der_nächsten_Frage(roundIndex, id1, (index) => result = index, null);

            //Assert.AreEqual(expectedQuestionIndex, result, "Index der nächsten Frage nicht korrekt ermittelt!");
        }

        [TestMethod]
        public void Index_der_nächsten_FrageTestNoNextQuestion()
        {
            const string id1 = "1";
            const string id2 = "2";

            const int roundCount = 2;
            const int roundIndex = 1;

            var indices1 = new List<int> { 1, 2 };
            var indices2 = new List<int> { 4, 5 };

            Assert.Fail();

            //var battleData = new BattleData
            //                     {
            //                         Spieler = new[]
            //                                      {
            //                                          new Spieler
            //                                              {
            //                                                  Id = id1, 
            //                                                  Antwortindizes = indices1
            //                                              },
            //                                          new Spieler
            //                                              {
            //                                                  Id = id2, 
            //                                                  Antwortindizes = indices2
            //                                              }
            //                                      }
            //                     };

            //battleData.Fragen_eintragen(new[] { new Frage(), new Frage() }, roundCount);

            //var battle = new Battle();
            //battle.Binden(battleData);
            var noQuestions = false;

            //battle.Index_der_nächsten_Frage(roundIndex, id1, null, () => noQuestions = true);

            Assert.IsTrue(noQuestions, "Es wurde nicht erkannt, dass keine Fragen mehr folgen!");
        }

        [TestMethod]
        public void Antwort_eintragenTest()
        {
            const string id = "1";
            const int index = 2;

            Assert.Fail();

            //var battleData = new BattleData
            //                     {
            //                         Spieler = new[]
            //                                       {
            //                                           new Spieler
            //                                               {
            //                                                   Id = id,
            //                                                   Antwortindizes = new List<int>()
            //                                               }
            //                                       }
            //                     };
            var battle = new Battle();
            //battle.Binden(battleData);

            //battle.Antwort_eintragen(index, id);

            //var result = battleData.Spieler.FirstOrDefault().Antwortindizes.FirstOrDefault();

            //Assert.AreEqual(index, result, "Antwortindex wurde nicht hinzugefügt!");
        }

        [TestMethod]
        public void GetAntwortindziesTest()
        {
            const string id1 = "1";
            const string id2 = "2";

            var indices1 = new List<int> { 1, 2, 3 };
            var indices2 = new List<int> { 4, 5, 6 };

            Assert.Fail();

            //var battleData = new BattleData
            //                     {
            //                         Spieler =
            //                             new[]
            //                                 {
            //                                     new Spieler
            //                                         {
            //                                             Id = id1, 
            //                                             Antwortindizes = indices1
            //                                         },
            //                                     new Spieler
            //                                         {
            //                                             Id = id2, 
            //                                             Antwortindizes = indices2
            //                                         }
            //                                 }
            //                     };

            var battle = new Battle();
            //battle.Binden(battleData);
            var privateObject = new PrivateObject(battle);

            var result = (List<int>)privateObject.Invoke("GetAntwortindzies", id1);

            Assert.AreEqual(indices1, result, "Liste der Antwortindizes nicht korrekt abgerufen!");
        }

        [TestMethod]
        public void Rundenergebnisse_aus_Antworten_berechnenTest()
        {
            const int RightAnswerIndex = 0;
            const int WrongAnswerIndex1 = 1;
            const int WrongAnswerIndex2 = 2;

            var battleData = new BattleData();
            battleData.Fragen_eintragen(new[]
                                      {
                                          new Frage(), 
                                          new Frage(), 
                                          new Frage(), 
                                          new Frage(), 
                                          new Frage(), 
                                          new Frage()
                                      }, 3);

            var battle = new Battle();
            battle.Binden(battleData);
            var privateObject = new PrivateObject(battle);

            var result = (List<int>)privateObject.Invoke("Rundenergebnisse_aus_Antworten_berechnen", new List<int>
                                                                                        {
                                                                                            RightAnswerIndex, 
                                                                                            RightAnswerIndex, 
                                                                                            WrongAnswerIndex1, 
                                                                                            WrongAnswerIndex2, 
                                                                                            RightAnswerIndex
                                                                                        });

            Assert.AreEqual(2, result[0], "Ergebnis der ersten Runde nicht korrekt ermittelt!");
            Assert.AreEqual(0, result[1], "Ergebnis der zweiten Runde nicht korrekt ermittelt!");
            Assert.AreEqual(1, result[2], "Ergebnis der dritten Runde nicht korrekt ermittelt!");
        }
    }
}
