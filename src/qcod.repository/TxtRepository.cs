﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using qcod.contracts.data;
using qcod.contracts.dialoge.runde;

namespace qcod.repository
{
    public class TxtRepository : IRepository
    {
        private BattleData _battleData;

        public TxtRepository()
        {
            _battleData = new BattleData();
            _battleData.Spieler = new[]
                {
                    new Spieler{Id="0", Name="Neo", Antworten = new List<Antworttyp>()},
                    new Spieler{Id="1", Name="Smith", Antworten = new List<Antworttyp>()}
                };

            _battleData.Fragen_eintragen(
                new[] {
                    new Frage {Text = "Welches Tier ist kein Säugetier?", Antwortoptionen = new[] {"Ameise", "Hund", "Katze", "Maus"}},
                    new Frage {Text = "Die ultimative Antwort?", Antwortoptionen = new[] {"42", "0815", "007", "110"}},
                    new Frage {Text = "1+1=?", Antwortoptionen = new[] {"2", "11", "10", "3"}},
                    new Frage {Text = "to sleep", Antwortoptionen = new[] {"schlafen", "schlagen", "schleifen", "schluchzen"}},
                    new Frage {Text = "gehen", Antwortoptionen = new[] {"to go", "to get", "to grab", "to gawk"}},
                    new Frage {Text = "Einwohnerreichste Stadt Deutschlands?", Antwortoptionen = new[] {"Berlin", "Hamburg", "München", "Essen"}}
                },
                3);
            _battleData.Status = Spielzustände.Angenommen;
        }

        public BattleData LoadBattle()
        {
            if (!File.Exists("battledata.txt")) JustStoreBattle(_battleData);

            using (var sr = new StreamReader("battledata.txt"))
            {
                _battleData = new BattleData();

                _battleData.AnzahlRunden = int.Parse(sr.ReadLine());
                _battleData.Fragen = new Frage[int.Parse(sr.ReadLine())];
                for (var i = 0; i < _battleData.Fragen.Length; i++)
                {
                    _battleData.Fragen[i] = new Frage();
                    _battleData.Fragen[i].Text = sr.ReadLine();
                    _battleData.Fragen[i].Antwortoptionen = sr.ReadLine().Split(',');
                }

                _battleData.Spieler = new Spieler[2];
                _battleData.Spieler[0] = new Spieler();
                _battleData.Spieler[0].Id = sr.ReadLine();
                _battleData.Spieler[0].Name = sr.ReadLine();
                var antworten = sr.ReadLine();
                _battleData.Spieler[0].Antworten = antworten != string.Empty
                                                            ? antworten
                                                                .Split(',')
                                                                .Select((s) => (Antworttyp)Enum.Parse(typeof(Antworttyp), s))
                                                                .ToList()
                                                            : new List<Antworttyp>();
                _battleData.Spieler[1] = new Spieler();
                _battleData.Spieler[1].Id = sr.ReadLine();
                _battleData.Spieler[1].Name = sr.ReadLine();
                antworten = sr.ReadLine();
                _battleData.Spieler[1].Antworten = antworten != string.Empty
                                                            ? antworten.
                                                            Split(',')
                                                            .Select((s) => (Antworttyp)Enum.Parse(typeof(Antworttyp), s)).ToList()
                                                            : new List<Antworttyp>();

                return _battleData;
            }
        }

        public void StoreBattle(BattleData battleData)
        {
            var currentBattleData = LoadBattle();

            battleData = MergeBattleData(currentBattleData, battleData);

            JustStoreBattle(battleData);
        }

        private void JustStoreBattle(BattleData battleData)
        {
            using (var sw = new StreamWriter("battledata.txt"))
            {
                sw.WriteLine(battleData.AnzahlRunden);
                sw.WriteLine(battleData.Fragen.Length);
                foreach (var f in battleData.Fragen)
                {
                    sw.WriteLine(f.Text);
                    sw.WriteLine(string.Join(",", f.Antwortoptionen));
                }
                sw.WriteLine(battleData.Spieler[0].Id);
                sw.WriteLine(battleData.Spieler[0].Name);
                sw.WriteLine(string.Join(",", battleData.Spieler[0].Antworten));
                sw.WriteLine(battleData.Spieler[1].Id);
                sw.WriteLine(battleData.Spieler[1].Name);
                sw.WriteLine(string.Join(",", battleData.Spieler[1].Antworten));
            }
        }

        internal BattleData MergeBattleData(BattleData source, BattleData target)
        {
            if (target.Spieler[0].Antworten.Count < source.Spieler[0].Antworten.Count)
            {
                target.Spieler[0].Antworten = source.Spieler[0].Antworten;
            }

            if (target.Spieler[1].Antworten.Count < source.Spieler[1].Antworten.Count)
            {
                target.Spieler[1].Antworten = source.Spieler[1].Antworten;
            }

            return target;
        }
    }
}
