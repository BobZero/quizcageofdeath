﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace qcod.ui.wpf.Converters
{
    internal class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility retVal;
            if (parameter is string)
            {
                var stringParameter = (parameter as string).ToLower();

                if (value is bool && ((bool)value))
                {
                    switch (stringParameter)
                    {
                        case "inversehidden":
                            retVal = Visibility.Hidden;
                            break;
                        case "inversecollapsed":
                            retVal = Visibility.Collapsed;
                            break;
                        default:
                            retVal = Visibility.Visible;
                            break;
                    }
                }
                else
                {
                    if (stringParameter.StartsWith("inverse"))
                    {
                        retVal = Visibility.Visible;
                    }
                    else if (stringParameter == "hidden")
                    {
                        retVal = Visibility.Hidden;
                    }
                    else
                    {
                        retVal = Visibility.Collapsed;
                    }
                }
            }
            else
            {
                if (value is bool && ((bool)value))
                {
                    retVal = Visibility.Visible;
                }
                else
                {
                    retVal = Visibility.Collapsed;
                }
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
