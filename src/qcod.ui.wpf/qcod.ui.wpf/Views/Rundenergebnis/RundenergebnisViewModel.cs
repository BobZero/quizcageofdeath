﻿using qcod.contracts.dialoge.battle;
using qcod.ui.wpf.Base;

namespace qcod.ui.wpf.Views.Rundenergebnis
{
    internal class RundenergebnisViewModel : BaseViewModel
    {
        internal RundenergebnisViewModel(Ergebnis ergebnis)
        {
            MeinPunktestand = ergebnis.MeinPunktestand;
            GegnerPunktestand = ergebnis.Gegnerpunktestand;
        }

        public int MeinPunktestand { get; private set; }

        public int GegnerPunktestand { get; private set; }
    }
}
