﻿using System.ComponentModel;

namespace qcod.ui.wpf.Views.MainWindow
{
    internal partial class MainWindowView
    {
        internal MainWindowView()
        {
            InitializeComponent();
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            (DataContext as MainWindowViewModel).Close();
        }
    }
}
