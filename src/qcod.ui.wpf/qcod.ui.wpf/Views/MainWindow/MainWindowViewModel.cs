﻿using System;

using qcod.ui.wpf.Base;

namespace qcod.ui.wpf.Views.MainWindow
{
    internal class MainWindowViewModel : BaseViewModel
    {
        private BaseViewModel _currentView;
        
        public BaseViewModel CurrentView
        {
            get
            {
                return _currentView;
            }
            internal set
            {
                if (_currentView == value)
                {
                    return;
                }

                _currentView = value; 

                RaisePropertyChanged();
            }
        }

        internal Action OnExitRequestedAction { get; set; }

        internal void Close()
        {
            OnExitRequestedAction();
        }
    }
}
