﻿using System;
using System.Diagnostics;
using System.Windows.Media;

using qcod.contracts.dialoge.runde;
using qcod.ui.wpf.Base;

namespace qcod.ui.wpf.Views.FragenInfo
{
    internal class FragenInfoViewModel : BaseViewModel
    {
        private SolidColorBrush _answer1Color;
        private SolidColorBrush _answer2Color;
        private SolidColorBrush _answer3Color;
        private SolidColorBrush _answer4Color;

        private bool _isAnswered;

        private int _maxProgress;

        private int _progress;

        private string _remainingSeconds;

        internal FragenInfoViewModel(
            contracts.dialoge.runde.FragenInfo fragenInfo,
            Action<Antworttyp> onAntwortauswertungRequested,
            Action onNächsteFrageRequested,
            Action onRundenabbruchRequested)
        {
            Fragetext = fragenInfo.Fragentext;
            FragenNr = fragenInfo.Fragennummer;
            AnzahlFragenInRunde = fragenInfo.AnzahlFragenInRunde;
            AntwortoptionenTexte = fragenInfo.Antwortoptionentexte;
            MaxProgress = 1;
            Progress = 0;

            CancelCommand = new BaseCommand(onRundenabbruchRequested);
            NextCommand = new BaseCommand(onNächsteFrageRequested);
            AnswerCommand = new GenericBaseCommand<string>(
                (parameter) => onAntwortauswertungRequested((Antworttyp)Enum.Parse(typeof(Antworttyp), parameter)));

            Answer1Color = new SolidColorBrush(Colors.Silver);
            Answer2Color = new SolidColorBrush(Colors.Silver);
            Answer3Color = new SolidColorBrush(Colors.Silver);
            Answer4Color = new SolidColorBrush(Colors.Silver);
            IsAnswered = true;
        }

        public GenericBaseCommand<string> AnswerCommand { get; private set; }

        public BaseCommand NextCommand { get; private set; }

        public BaseCommand CancelCommand { get; private set; }

        public string Fragetext { get; private set; }

        public int FragenNr { get; private set; }

        public int AnzahlFragenInRunde { get; private set; }

        public string[] AntwortoptionenTexte { get; private set; }

        public SolidColorBrush Answer1Color
        {
            get
            {
                return _answer1Color;
            }
            private set
            {
                if (Equals(value, _answer1Color))
                {
                    return;
                }

                _answer1Color = value;
                RaisePropertyChanged();
            }
        }

        public SolidColorBrush Answer2Color
        {
            get
            {
                return _answer2Color;
            }
            set
            {
                if (Equals(value, _answer2Color))
                {
                    return;
                }

                _answer2Color = value;
                RaisePropertyChanged();
            }
        }

        public SolidColorBrush Answer3Color
        {
            get
            {
                return _answer3Color;
            }
            set
            {
                if (Equals(value, _answer3Color))
                {
                    return;
                }

                _answer3Color = value;
                RaisePropertyChanged();
            }
        }

        public SolidColorBrush Answer4Color
        {
            get
            {
                return _answer4Color;
            }
            set
            {
                if (Equals(value, _answer4Color))
                {
                    return;
                }

                _answer4Color = value;
                RaisePropertyChanged();
            }
        }

        public bool IsAnswered
        {
            get
            {
                return _isAnswered;
            }
            private set
            {
                if (value.Equals(_isAnswered))
                {
                    return;
                }

                _isAnswered = value;
                RaisePropertyChanged();
            }
        }

        public int MaxProgress
        {
            get
            {
                return _maxProgress;
            }
            set
            {
                if (value == _maxProgress)
                {
                    return;
                }

                _maxProgress = value;
                RaisePropertyChanged();
            }
        }

        public int Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                if (value == _progress)
                {
                    return;
                }

                _progress = value;
                RaisePropertyChanged();
            }
        }

        public string RemainingSeconds
        {
            get
            {
                return _remainingSeconds;
            }
            private set
            {
                if (value == _remainingSeconds)
                {
                    return;
                }
                _remainingSeconds = value;
                RaisePropertyChanged();
            }
        }

        internal void ShowAntwort(AntwortInfo antwortInfo)
        {
            IsAnswered = false;
            ShowVerbleibendeZeit(TimeSpan.Zero, TimeSpan.Zero);

            if (antwortInfo.EigeneAntwort == antwortInfo.RichtigeAntwort)
            {
                switch (antwortInfo.EigeneAntwort)
                {
                    case Antworttyp.Antwort1:
                        Answer1Color = new SolidColorBrush(Colors.LawnGreen);
                        break;
                    case Antworttyp.Antwort2:
                        Answer2Color = new SolidColorBrush(Colors.LawnGreen);
                        break;
                    case Antworttyp.Antwort3:
                        Answer3Color = new SolidColorBrush(Colors.LawnGreen);
                        break;
                    case Antworttyp.Antwort4:
                        Answer4Color = new SolidColorBrush(Colors.LawnGreen);
                        break;
                }
            }
            else
            {
                switch (antwortInfo.EigeneAntwort)
                {
                    case Antworttyp.Antwort1:
                        Answer1Color = new SolidColorBrush(Colors.Red);
                        break;
                    case Antworttyp.Antwort2:
                        Answer2Color = new SolidColorBrush(Colors.Red);
                        break;
                    case Antworttyp.Antwort3:
                        Answer3Color = new SolidColorBrush(Colors.Red);
                        break;
                    case Antworttyp.Antwort4:
                        Answer4Color = new SolidColorBrush(Colors.Red);
                        break;
                }

                switch (antwortInfo.RichtigeAntwort)
                {
                    case Antworttyp.Antwort1:
                        Answer1Color = new SolidColorBrush(Colors.Yellow);
                        break;
                    case Antworttyp.Antwort2:
                        Answer2Color = new SolidColorBrush(Colors.Yellow);
                        break;
                    case Antworttyp.Antwort3:
                        Answer3Color = new SolidColorBrush(Colors.Yellow);
                        break;
                    case Antworttyp.Antwort4:
                        Answer4Color = new SolidColorBrush(Colors.Yellow);
                        break;
                }
            }
        }

        public void ShowVerbleibendeZeit(TimeSpan verbleibendeZeit, TimeSpan gesamtZeit)
        {
            MaxProgress = (int)gesamtZeit.TotalSeconds;
            Progress = MaxProgress - (int)verbleibendeZeit.TotalSeconds;

            RemainingSeconds = string.Format("Verbleibende Zeit: {0} Sekunden", (int)verbleibendeZeit.TotalSeconds);
        }
    }
}
