﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

using qcod.ui.wpf.Base;
using qcod.ui.wpf.Views.Rundenergebnis;

namespace qcod.ui.wpf.Views.BattleInfo
{
    internal class BattleInfoViewModel : BaseViewModel
    {
        internal BattleInfoViewModel(
            contracts.dialoge.battle.BattleInfo battleinfo, 
            Action onFightRequestedAction, 
            Action onSurrenderRequested, 
            Action onRefreshRequestedAction)
        {
            MeinName = battleinfo.MeinName;
            GegnerName = battleinfo.Gegnername;

            MeinSpielergebnis = battleinfo.Spielergebnis.MeinPunktestand;
            GegnerSpielergebnis = battleinfo.Spielergebnis.Gegnerpunktestand;

            Rundenergebnisse = new ObservableCollection<RundenergebnisViewModel>
                (battleinfo.Rundenergebnisse.Select(re => new RundenergebnisViewModel(re)));

            FightCommand = new BaseCommand(onFightRequestedAction);
            SurrenderCommand = new BaseCommand(onSurrenderRequested);
            RefreshCommand = new BaseCommand(onRefreshRequestedAction);
        }
        
        public BaseCommand FightCommand { get; private set; }
        public BaseCommand SurrenderCommand { get; private set; }
        public BaseCommand RefreshCommand { get; private set; }

        public string MeinName { get; private set; }
        public string GegnerName { get; private set; }

        public int MeinSpielergebnis { get; private set; }
        public int GegnerSpielergebnis { get; private set; }

        public ObservableCollection<RundenergebnisViewModel> Rundenergebnisse { get; private set; }
    }
}
