﻿using System;
using System.Windows;

using qcod.contracts.dialoge.battle;
using qcod.contracts.dialoge.runde;
using qcod.ui.wpf.Views.BattleInfo;
using qcod.ui.wpf.Views.FragenInfo;
using qcod.ui.wpf.Views.MainWindow;

namespace qcod.ui.wpf
{
    public class UserInterface : IRundenDialog, IBattleDialog
    {
        private readonly Window _mainWindow;
        private readonly MainWindowViewModel _mainWindowViewModel;

        public event Action<Antworttyp> AntwortauswertungRequested;
        public event Action NächsteFrageRequested;
        public event Action RundenabbruchRequested;

        public event Action RefreshRequested;
        public event Action FightRequested;
        public event Action SurrenderRequested;
        public event Action ExitRequested;

        public UserInterface()
        {
            _mainWindow = new MainWindowView();
            _mainWindowViewModel = new MainWindowViewModel();

            _mainWindow.DataContext = _mainWindowViewModel;
        }

        public void ShowFrage(FragenInfo frageninfo)
        {
            CheckDispatcher(() =>
            {
                _mainWindowViewModel.CurrentView = new FragenInfoViewModel(
                    frageninfo,
                    AntwortauswertungRequested,
                    NächsteFrageRequested,
                    RundenabbruchRequested);
            });
        }

        private void CheckDispatcher(Action onAccessGranted)
        {
            if (_mainWindow.CheckAccess())
            {
                onAccessGranted();
            }
            else
            {
                _mainWindow.Dispatcher.Invoke(onAccessGranted);
            }
        }

        public void ShowAntworten(AntwortInfo antwortinfo)
        {
            CheckDispatcher(
                () =>
                {
                    (_mainWindowViewModel.CurrentView as FragenInfoViewModel).ShowAntwort(antwortinfo);
                });
        }

        public void Show(BattleInfo battleinfo)
        {
            CheckDispatcher(() =>
            {
                _mainWindowViewModel.CurrentView = new BattleInfoViewModel(
                    battleinfo,
                    FightRequested,
                    SurrenderRequested,
                    RefreshRequested);

                if (_mainWindow.IsVisible)
                {
                    return;
                }

                _mainWindowViewModel.OnExitRequestedAction = ExitRequested;
                _mainWindow.Show();
            });
        }

        public void ShowVerbleibendeZeit(TimeSpan verbleibendeZeit, TimeSpan gesamtZeit)
        {
            CheckDispatcher(
                () =>
                {
                    if (_mainWindowViewModel.CurrentView is FragenInfoViewModel)
                    {
                        (_mainWindowViewModel.CurrentView as FragenInfoViewModel).ShowVerbleibendeZeit(verbleibendeZeit, gesamtZeit);
                    }
                });
        }
    }
}
