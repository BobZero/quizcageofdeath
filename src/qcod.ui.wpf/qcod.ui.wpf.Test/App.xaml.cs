﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using qcod.contracts.data;
using qcod.contracts.dialoge.battle;
using qcod.contracts.dialoge.runde;

namespace qcod.ui.wpf.Test
{
    public partial class App
    {
        private CancellationTokenSource _cancellationTokenSource;
        private UserInterface _userInterface;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            _userInterface = new UserInterface();
            _userInterface.FightRequested += OnFightRequested;
            _userInterface.ExitRequested += Current.Shutdown;
            _userInterface.AntwortauswertungRequested += OnAntwortauswertungRequested;
            _userInterface.NächsteFrageRequested += OnNächsteFrageRequested;
            _userInterface.RefreshRequested += OnRefreshRequested;
            _userInterface.RundenabbruchRequested += OnRundenabbruchRequested;
            _userInterface.SurrenderRequested += OnSurrenderRequested;

            _userInterface.Show(GenerateBattleInfo());
        }

        private static BattleInfo GenerateBattleInfo()
        {
            var random = new Random(DateTime.Now.Second);
            var rounds = random.Next(1, 6);
            var rundenergebnisse = new List<Ergebnis>();

            for (var i = 0; i < rounds; i++)
            {
                var myRoundResult = random.Next(0, 4);
                var enemyRoundResult = random.Next(0, 4);

                rundenergebnisse.Add(new Ergebnis
                                         {
                                             Gegnerpunktestand = enemyRoundResult,
                                             MeinPunktestand = myRoundResult
                                         });
            }

            var gameResult = new Ergebnis
                                 {
                                     MeinPunktestand = rundenergebnisse.Sum(r => r.MeinPunktestand),
                                     Gegnerpunktestand = rundenergebnisse.Sum(r => r.Gegnerpunktestand)
                                 };

            return new BattleInfo
                       {
                           MeinName = "Mein Name_" + DateTime.Now.ToLongTimeString(),
                           Gegnername = "Gegnername_" + DateTime.Now.ToLongTimeString(),
                           Rundenergebnisse = rundenergebnisse.ToArray(),
                           Spielergebnis = gameResult,
                           Spielzustand = Spielzustände.Angenommen
                       };
        }

        private void OnSurrenderRequested()
        {
            _userInterface.Show(GenerateBattleInfo());
        }

        private void OnRundenabbruchRequested()
        {
            _cancellationTokenSource.Cancel();
            _userInterface.Show(GenerateBattleInfo());
        }

        private void OnRefreshRequested()
        {
            _userInterface.Show(GenerateBattleInfo());
        }

        private void OnNächsteFrageRequested()
        {
            _userInterface.ShowFrage(GenerateFragenInfo());
            SimulateProgress();
        }

        private void OnAntwortauswertungRequested(Antworttyp antwort)
        {
            _cancellationTokenSource.Cancel();
            
            _userInterface.ShowAntworten(new AntwortInfo
                                             {
                                                 EigeneAntwort = antwort,
                                                 RichtigeAntwort = Antworttyp.Antwort4
                                             });
        }

        private static FragenInfo GenerateFragenInfo()
        {
            return new FragenInfo
                       {
                           AnzahlFragenInRunde = 3,
                           Fragennummer = 1,
                           Fragentext = "Kekse?",
                           Antwortoptionentexte = new[] { "Ja", "Nein", "Vielleicht", "Weiß nicht" }
                       };
        }

        private void OnFightRequested()
        {
            _userInterface.ShowFrage(GenerateFragenInfo());

            SimulateProgress();
        }

        private async void SimulateProgress()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            await Task.Run(
                () =>
                {
                    const int MaxSeconds = 15;

                    for (var i = 0; i <= MaxSeconds; i++)
                    {
                        if (_cancellationTokenSource.Token.IsCancellationRequested)
                        {
                            return;
                        }

                        _userInterface.ShowVerbleibendeZeit(new TimeSpan(0, 0, 0, MaxSeconds - i), new TimeSpan(0, 0, 0, MaxSeconds));

                        Thread.Sleep(1500);
                    }

                    OnNächsteFrageRequested();
                });
        }
    }
}
